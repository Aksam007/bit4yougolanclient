package Wallet

type WalletSendRequest struct {
	Iso         string      `json:"iso"`
	Quantity    float64     `json:"quantity"`
	Address     string      `json:"address"`
	ClientID    string `json:"clientId"`
	TimingForce string `json:"timingForce"`
}
