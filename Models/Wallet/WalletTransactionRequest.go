package Wallet

type WalletTransactionRequest struct {
	Iso         string      `json:"iso"`
	Simulation  bool        `json:"simulation"`
	ClientID    string `json:"clientId"`
	TimingForce string `json:"timingForce"`
}
