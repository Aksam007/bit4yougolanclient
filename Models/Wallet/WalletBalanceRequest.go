package Wallet

type WalletBalanceRequest struct {
	Simulation  bool        `json:"simulation"`
	ClientID    string `json:"clientId"`
	TimingForce string `json:"timingForce"`
}
