package Wallet

type WalletTransactionResponse struct {
	Balance string `json:"balance"`
	Pages   int    `json:"pages"`
	Txs     []struct {
		Txid          string      `json:"txid"`
		Block         interface{} `json:"block"`
		Confirmations interface{} `json:"confirmations"`
		Fee           string      `json:"fee"`
		Time          int         `json:"time"`
		Quantity      string      `json:"quantity"`
		Type          string      `json:"type"`
		Meta          struct {
			Pending   bool    `json:"pending"`
			PaidValue float64 `json:"paid_value"`
			PaidAsset string  `json:"paid_asset"`
			FeeAsset  string  `json:"fee_asset"`
			Market    string  `json:"market"`
		} `json:"meta"`
	} `json:"txs"`
}
