package Wallet


type WalletBalanceResponse []struct {
	Iso        string `json:"iso"`
	Name       string `json:"name"`
	Tx         string `json:"tx"`
	TxEnabled  bool   `json:"tx_enabled,omitempty"`
	Erc20      bool   `json:"erc20,omitempty"`
	Balance    string `json:"balance"`
	TxExplorer string `json:"tx_explorer,omitempty"`
	Digits     int    `json:"digits,omitempty"`
}
