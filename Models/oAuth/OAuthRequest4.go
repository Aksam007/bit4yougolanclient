package oauth

type OAuthRequest struct {
	GrantType string `json:"grant_type"`
	Scope     string `json:"scope"`
	Username  string `json:"username"`
	Password  string `json:"password"`
}
