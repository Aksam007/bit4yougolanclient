package oauth

import "time"

type ApiUserInfo struct {
	Sub          int       `json:"sub"`
	Iss          string    `json:"iss"`
	Aud          string    `json:"aud"`
	Iat          int       `json:"iat"`
	AuthTime     int       `json:"auth_time"`
	Verified     bool      `json:"verified"`
	IsChild      bool      `json:"is_child"`
	FullLocked   bool      `json:"full_locked"`
	Name         string    `json:"name"`
	FamilyName   string    `json:"family_name"`
	GivenName    string    `json:"given_name"`
	MiddleName   string    `json:"middle_name"`
	Gender       string    `json:"gender"`
	Birthdate    string    `json:"birthdate"`
	Birthplace   string    `json:"birthplace"`
	Zoneinfo     string    `json:"zoneinfo"`
	Locale       string    `json:"locale"`
	Currency     string    `json:"currency"`
	UpdatedAt    time.Time `json:"updated_at"`
	IDNumber     string    `json:"id_number"`
	IDExpiration string    `json:"id_expiration"`
	Nationality  string    `json:"nationality"`
}
