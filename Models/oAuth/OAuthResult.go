package oauth


type OAuthResult struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	IDToken     string `json:"id_token"`
	ExpiresIn   int    `json:"expires_in"`
	AuthExp     int    `json:"auth_exp"`
}
