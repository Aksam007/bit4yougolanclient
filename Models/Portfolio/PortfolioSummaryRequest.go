package Portfolio

type PortfolioSummaryRequest struct {
	Simulation  bool   `json:"simulation"`
	ClientID    string `json:"clientId"`
	TimingForce string `json:"timingForce"`
}
