package Portfolio


type PortfolioHistoryResponse []struct {
	ID           int    `json:"id"`
	Market       string `json:"market"`
	BaseCurrency string `json:"baseCurrency"`
	Invested     string `json:"invested"`
	ClosedAmount string `json:"closed_amount"`
	Quantity     string `json:"quantity"`
	Fee          string `json:"fee"`
	OpenTime     int    `json:"open_time"`
	CloseTime    int    `json:"close_time"`
}
