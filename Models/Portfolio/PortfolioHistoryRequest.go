package Portfolio

type PortfolioHistoryRequest struct {
	Simulation  bool        `json:"simulation"`
	ClientID    interface{} `json:"clientId"`
	TimingForce interface{} `json:"timingForce"`
}
