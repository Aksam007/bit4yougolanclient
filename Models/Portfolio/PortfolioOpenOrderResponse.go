package Portfolio

type PortfolioOpenOrderResponse []struct {
	ID                int         `json:"id"`
	Type              string      `json:"type"`
	ExecuteAt         string      `json:"execute_at"`
	RemainingQuantity string      `json:"remaining_quantity"`
	RemainingIso      string      `json:"remaining_iso"`
	IsOpening         int         `json:"isOpening"`
	Market            string      `json:"market"`
	BaseCurrency      string      `json:"baseCurrency"`
	Invested          string      `json:"invested"`
	Quantity          string      `json:"quantity"`
	OpenTime          int         `json:"open_time"`
	CloseTime         interface{} `json:"close_time"`
}
