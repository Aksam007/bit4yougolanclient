package Portfolio


type PortfolioSummaryResponse struct {
	Items []struct {
		ID       int    `json:"id"`
		BaseIso  string `json:"base_iso"`
		Market   string `json:"market"`
		Invested string `json:"invested"`
		Quantity string `json:"quantity"`
		OpenTime int    `json:"open_time"`
	} `json:"items"`
	Wallet string `json:"wallet"`
}
