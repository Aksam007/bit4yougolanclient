package Portfolio

type PortfolioCancelCloseResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}
