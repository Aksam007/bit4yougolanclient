package Market

type MarketListResponse []struct {
	Iso       string `json:"iso"`
	Name      string `json:"name"`
	Precision int    `json:"precision"`
	Value     string `json:"value"`
	Change    string `json:"change"`
	Spread    string `json:"spread"`
	Supply    string `json:"supply"`
	Ath       struct {
		Time  int    `json:"time"`
		Value string `json:"value"`
	} `json:"ath,omitempty"`
}
