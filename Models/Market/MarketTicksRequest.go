package Market

type MarketTicksRequest struct {
	Market      string      `json:"market"`
	Interval    int         `json:"interval"`
	ClientID    string `json:"clientId"`
	TimingForce string `json:"timingForce"`
}