package Market

type MarketHistoryRequest struct {
	Market      string      `json:"market"`
	Limit       int         `json:"limit"`
	From        string      `json:"from"`
	To          string      `json:"to"`
	ClientID    string `json:"clientId"`
	TimingForce string `json:"timingForce"`
}