package Market


type MarketTicksResponse []struct {
	Time         int    `json:"time"`
	Open         string `json:"open"`
	Close        string `json:"close"`
	Low          string `json:"low"`
	High         string `json:"high"`
	Volume       string `json:"volume"`
	MarketVolume string `json:"marketVolume"`
}
