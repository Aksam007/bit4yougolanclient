package Market

type MarketHistoryResponse []struct {
	Buy      bool   `json:"buy"`
	ID       string `json:"id"`
	Quantity string `json:"quantity"`
	Rate     string `json:"rate"`
	Time     int    `json:"time"`
}
