package Market

type MarketOrderBookResponse struct {
	Ask []struct {
		Quantity string `json:"quantity"`
		Rate     string `json:"rate"`
		I        int    `json:"i"`
	} `json:"ask"`
	Bid []struct {
		Quantity string `json:"quantity"`
		Rate     string `json:"rate"`
		I        int    `json:"i"`
	} `json:"bid"`
}
