package Order


type OrderInfoRequest struct {
	Txid        string      `json:"txid"`
	Simulation  bool        `json:"simulation"`
	ClientID    string `json:"clientId"`
	TimingForce string `json:"timingForce"`
}


