package Order

type OrderList struct {
	Page        int         `json:"page"`
	Limit       int         `json:"limit"`
	Market      string      `json:"market"`
	Simulation  bool        `json:"simulation"`
	ClientID    string  	`json:"clientId"`
	TimingForce string 		`json:"timingForce"`
}