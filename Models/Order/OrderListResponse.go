package Order

type OrderListResponse struct {
	Txid            string `json:"txid"`
	Type            string `json:"type"`
	Market          string `json:"market"`
	IsOpen          bool   `json:"isOpen"`
	RequestedRate   string `json:"requested_rate"`
	Quantity        string `json:"quantity"`
	BaseQuantity    string `json:"base_quantity"`
	BlockedQuantity string `json:"blocked_quantity"`
	Remaining       struct {
		Quantity string `json:"quantity"`
		Iso      string `json:"iso"`
	} `json:"remaining"`
	Fee struct {
		Quantity string `json:"quantity"`
		Iso      string `json:"iso"`
	} `json:"fee"`
	Position struct {
		ID        interface{} `json:"id"`
		HistoryID interface{} `json:"history_id"`
	} `json:"position"`
	OpenTime   int `json:"open_time"`
	UpdateTime int `json:"update_time"`
}
