package Order


type OrderCancelRequest struct {
	Txid        string      `json:"txid"`
	Simulation  bool        `json:"simulation"`
	ClientID    interface{} `json:"clientId"`
	TimingForce interface{} `json:"timingForce"`
}
