package Order


type OrderCreateRequest struct {
	Txid        interface{} `json:"txid"`
	Market      string      `json:"market"`
	Type        string      `json:"type"`
	Quantity    int         `json:"quantity"`
	QuantityIso string      `json:"quantity_iso"`
	Rate        float64     `json:"rate"`
	Simulation  bool        `json:"simulation"`
	ClientID    string `json:"clientId"`
	TimingForce string `json:"timingForce"`
}
