package main

import (
	"Bit4YouGoClient/Models/Market"
	"Bit4YouGoClient/Models/Order"
	"Bit4YouGoClient/Models/Portfolio"
	"Bit4YouGoClient/Models/Wallet"
	"Bit4YouGoClient/Services"
	"github.com/joho/godotenv"
	"log"
	"os"
)

func goDotEnvVariable(key string) string {
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	return os.Getenv(key)
}

func main()  {


	Services.UserInfo()
	Services.OrderList(Order.OrderList{
		Page:        0,
		Limit:       10,
		Market:      "USDT-BTC",
		Simulation:  true,
	})
	
	Services.OrderInfo(Order.OrderInfoRequest{
		Txid:        "db78faa89f08062bfebeacb51365fadb08b63da6",
		Simulation:  true,
		//ClientID:    "",
		//TimingForce: "",
	})
	
	Services.OrderPending(Order.OrderPendingRequest{
		Simulation:  true,
		//ClientID:    nil,
		//TimingForce: nil,
	})

	Services.OrderCreate(Order.OrderCreateRequest{
		Market:      "USDT-BTC",
		Type:        "buy",
		Quantity:    10,
		QuantityIso: "BTC",
		Rate:        1.5,
		Simulation:  true,
		//ClientID:    "",
		//TimingForce: "",
	})
	
	Services.OrderCancel(Order.OrderCancelRequest{
		Txid:        "30eb76fa30154f48883446e274e7a0bd",
		Simulation:  true,
		//ClientID:    nil,
		//TimingForce: nil,
	})
	
	Services.WalletBalance(Wallet.WalletBalanceRequest{
		Simulation:  true,
		//ClientID:    "",
		//TimingForce: "",
	})
	
	Services.WalletTransaction(Wallet.WalletTransactionRequest{
		Iso:         "BTC",
		Simulation:  true,
		//ClientID:    "",
		//TimingForce: "",
	})
	
	Services.WalletSend(Wallet.WalletSendRequest{
		Iso:         "BTC",
		Quantity:    1.5,
		Address:     "1CK6KHY6MHgYvmRQ4PAafKYDrg1eaaaaaa",
		//ClientID:    nil,
		///timingForce: nil,
	})
	
	Services.MarketList()
	
	Services.MarketSummaries()
	
	Services.MarketTicks(Market.MarketTicksRequest{
		Market:      "USDT-BTC",
		Interval:    60,
		//ClientID:    "",
		//TimingForce: "",
	})
	
	Services.MarketOrderBooks(Market.MarketOrderBookRequest{
		Market:      "USDT-BTC",
		Limit:       50,
		State:       true,
		//ClientID:    "",
		//TimingForce: "",
	})
	
	Services.MarketHistory(Market.MarketHistoryRequest{
		Market:      "USDT-BTC",
		Limit:       50,
		From:        "String",
		To:          "String",
		//ClientID:    "",
		//TimingForce: "",
	})

	Services.PortfolioSummary(Portfolio.PortfolioSummaryRequest{
		Simulation:  true,
		//ClientID:    "",
		//TimingForce: "",
	})

	Services.PortfolioOpenOrder(Portfolio.PortfolioOpenOrder{
		Simulation:  true,
		//ClientID:    "",
		//TimingForce: "",
	})

	Services.PortfolioHistory(Portfolio.PortfolioHistoryRequest{
		Simulation:  true,
		//ClientID:    nil,
		//TimingForce: nil,
	})

	Services.PortfolioCreateOrder(Portfolio.PortfolioCreateOrder{
		Market:      "USDT-BTC",
		Quantity:    0.55,
		Rate:        355.6,
		Simulation:  true,
		ClientID:    nil,
		TimingForce: nil,
	})
	
	Services.PortfolioCancelOrder(Portfolio.PortfolioCancelCloseOrderRequest{
		ID:          178015,
		Simulation:  true,
		//ClientID:    "",
		//TimingForce: "",
	})

	Services.PortfolioCloseOrder(Portfolio.PortfolioCancelCloseOrderRequest{
		ID:          177170,
		Simulation:  true,
		//ClientID:    "",
		//TimingForce: "",
	})
}
