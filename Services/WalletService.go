package Services

import (
	"Bit4YouGoClient/Models/Wallet"
	"encoding/json"
	"log"
)

func WalletBalance(walletBalanceRequest Wallet.WalletBalanceRequest) Wallet.WalletBalanceResponse {
	token:=GetAccessToken()
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"wallet/balances",token,"POST", walletBalanceRequest)
	var result Wallet.WalletBalanceResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}

func WalletTransaction(walletTransactionRequest Wallet.WalletTransactionRequest) Wallet.WalletTransactionResponse {
	token:=GetAccessToken()
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"wallet/transactions",token,"POST", walletTransactionRequest)
	var result Wallet.WalletTransactionResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}

func WalletSend(walletSendRequest Wallet.WalletSendRequest) Wallet.WalletTransactionResponse {
	token:=GetAccessToken()
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"wallet/send",token,"POST", walletSendRequest)
	var result Wallet.WalletTransactionResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}
