package Services

import (
	"Bit4YouGoClient/Models/Market"
	"encoding/json"
	"log"
)

func MarketList() Market.MarketListResponse {
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"market/list","","GET",nil)
	var result Market.MarketListResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}

func MarketSummaries() Market.MarketSummaryResponse {
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"market/summaries","","GET",nil)
	var result Market.MarketSummaryResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}

func MarketTicks(marketTicksRequest Market.MarketTicksRequest) Market.MarketTicksResponse {
	token:=GetAccessToken()
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"market/ticks",token,"POST", marketTicksRequest)
	var result Market.MarketTicksResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}

func MarketOrderBooks(marketOrderBookRequest Market.MarketOrderBookRequest) Market.MarketOrderBookResponse {
	token:=GetAccessToken()
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"market/orderbook",token,"POST", marketOrderBookRequest)
	var result Market.MarketOrderBookResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}

func MarketHistory(marketHistoryRequest Market.MarketHistoryRequest) Market.MarketHistoryResponse  {
	token:=GetAccessToken()
	response:=ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"market/history",token,"POST", marketHistoryRequest)
	var result Market.MarketHistoryResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}
