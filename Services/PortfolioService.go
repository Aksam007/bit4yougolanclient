package Services

import (
	"Bit4YouGoClient/Models/Portfolio"
	"encoding/json"
	"log"
)

func PortfolioSummary(portfolioSummaryRequest Portfolio.PortfolioSummaryRequest) Portfolio.PortfolioSummaryResponse {
	token:=GetAccessToken()
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"portfolio/list",token,"POST", portfolioSummaryRequest)
	var result Portfolio.PortfolioSummaryResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}

func PortfolioOpenOrder(portfolioOpenOrder Portfolio.PortfolioOpenOrder) Portfolio.PortfolioOpenOrderResponse {
	token:=GetAccessToken()
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"portfolio/open-orders",token,"POST", portfolioOpenOrder)
	var result Portfolio.PortfolioOpenOrderResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}

func PortfolioHistory(portfolioHistoryRequest Portfolio.PortfolioHistoryRequest) Portfolio.PortfolioHistoryResponse {
	token:=GetAccessToken()
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"portfolio/history",token,"POST", portfolioHistoryRequest)
	var result Portfolio.PortfolioHistoryResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}

func PortfolioCreateOrder(portfolioCreateOrder Portfolio.PortfolioCreateOrder) Portfolio.PortfolioCreateOrderResponse {
	token:=GetAccessToken()
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"portfolio/create-order",token,"POST", portfolioCreateOrder)
	var result Portfolio.PortfolioCreateOrderResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}

func PortfolioCloseOrder(portfolioCancelOrderRequest Portfolio.PortfolioCancelCloseOrderRequest) Portfolio.PortfolioCancelCloseResponse {
	token:=GetAccessToken()
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"portfolio/close",token,"POST", portfolioCancelOrderRequest)
	var result Portfolio.PortfolioCancelCloseResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}

func PortfolioCancelOrder(portfolioCancelOrderRequest Portfolio.PortfolioCancelCloseOrderRequest) Portfolio.PortfolioCancelCloseResponse {
	token:=GetAccessToken()
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"portfolio/cancel-order",token,"POST", portfolioCancelOrderRequest)
	var result Portfolio.PortfolioCancelCloseResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}
