package Services

import (
	"bytes"
	json "encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

func ProcessHttpRequest(url string,accesstoken string,method string,data interface{}) []byte   {

	jsonPyaload, err := json.Marshal(data)
	if err != nil {
		log.Fatal(err)
	}

	var req *http.Request


	var er error
	if strings.EqualFold(method, "GET") || strings.EqualFold(method, "") {
		req, er = http.NewRequest(method, url,nil)
		if accesstoken!="" {
			req.Header.Add("Authorization","bearer "+accesstoken)
		}
		req.Header.Add("Content-Type","application/json")
	} else {
		req, er = http.NewRequest(method, url,bytes.NewReader(jsonPyaload))
		req.Header.Add("Content-Type","application/json")
		req.Header.Add("Authorization","bearer "+accesstoken)
	}

	if er != nil {
		// we couldn't parse the URL.
		log.Fatal(er)
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	return bodyBytes
}
