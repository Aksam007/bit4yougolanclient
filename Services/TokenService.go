package Services

import (
	oauth "Bit4YouGoClient/Models/oAuth"
	json "encoding/json"
	"github.com/joho/godotenv"
	"log"
	"os"
)
func goDotEnvVariable(key string) string {
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	return os.Getenv(key)
}

func GetAccessToken() string  {

	data:=oauth.OAuthRequest{
		GrantType: goDotEnvVariable("grant_type"),
		Scope:     goDotEnvVariable("scope"),
		Username:  goDotEnvVariable("ClientId"),
		Password:  goDotEnvVariable("clientSecret"),
	}

	response := ProcessHttpRequest(goDotEnvVariable("AccessTokenUrl")+"token","","POST", data)

	var result oauth.OAuthResult
	json.Unmarshal(response,&result)
	//log.Println(string(response))
	return result.AccessToken
}

func UserInfo() oauth.ApiUserInfo {
	token:=GetAccessToken()
	response := ProcessHttpRequest(goDotEnvVariable("AccessTokenUrl")+"userinfo",token,"GET",nil)
	var result oauth.ApiUserInfo
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}
