package Services

import (
	"Bit4YouGoClient/Models/Order"
	"encoding/json"
	"log"
)

func OrderList(orderListRequest Order.OrderList) Order.OrderListResponse {
	token:=GetAccessToken()
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"order/list",token,"POST", orderListRequest)
	var result Order.OrderListResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}

func OrderInfo(orderInfoRequest Order.OrderInfoRequest) Order.OrderInfoResponse {
	token:=GetAccessToken()
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"order/info",token,"POST", orderInfoRequest)
	var result Order.OrderInfoResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}

func OrderPending(orderPendingRequest Order.OrderPendingRequest) Order.OrderPendingResponse {
	token:=GetAccessToken()
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"order/pending",token,"POST", orderPendingRequest)
	var result Order.OrderPendingResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}

func OrderCreate(orderCreateRequest Order.OrderCreateRequest) Order.OrderCreateResponse {
	token:=GetAccessToken()
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"order/create",token,"POST", orderCreateRequest)
	var result Order.OrderCreateResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}

func OrderCancel(orderCancelRequest Order.OrderCancelRequest) Order.OrderCancelResponse {
	token:=GetAccessToken()
	response := ProcessHttpRequest(goDotEnvVariable("ApiUrl")+"order/cancel",token,"POST", orderCancelRequest)
	var result Order.OrderCancelResponse
	json.Unmarshal(response,&result)
	log.Println(string(response))
	return result
}
